<div align="center">

# nvm-cn

[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/ccmywish)

</div>

## 维护

<2023-05-01> 此仓库现在切换使用Gitee官方管理的`nvm`镜像，每日更新，最多只比上游晚一天。因此现在不再需要依赖我手动更新。(我已为此手动更新两年，至少每两周一次)

<2022-11-07> 此仓库最开始只是方便我自己临时使用，跟随[RubyKids]的其他项目一起公开出来了。但目前我很少使用NodeJS，对其生态不是很熟悉，如果出现bug,issue，希望您可以参与帮助。此仓库**2022年全年有2000+个独立IP访问，您的参与将会使很多人受益**。如果您愿意长时间维护这个项目，请联系我，我将很乐意为你打开相关权限。[RubyKids]里的其他项目也非常乐意接受维护，如果您认同[Woody's voice box哲学(请参考RubyKids的介绍)](https://gitee.com/RubyKids)，您可以申请加入该组织，并分享相关的项目于组织中。

<br>

## 安装

`bash -c "$(curl -fsSL https://gitee.com/RubyKids/nvm-cn/raw/main/install.sh)"`

## 卸载
`bash -c "$(curl -fsSL https://gitee.com/RubyKids/nvm-cn/raw/main/uninstall.sh)"`


## 使用

```bash
nvm ls

# 列出所有可安装版本
nvm ls-remote

# 安装某个版本Node
nvm install lts/fermium
nvm install v12.20.1
nvm install v15.5.1

# 切换Node版本
nvm use system
nvm use 14.15    # 不用全部打出版本号

# 更新nvm
nvm-update
```

## npm 换源

```bash
# 查看配置
npm config ls

npm config set registry https://registry.npmmirror.com
```

[RubyKids]: https://gitee.com/RubyKids
